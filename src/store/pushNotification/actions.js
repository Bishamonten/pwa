
export function addSwRegistration ({ commit }, swRegistration) {
  commit('ADD_SW_REGISTRATION', swRegistration)
}

export function addSubscription ({ commit }, subscription) {
  commit('ADD_SUBSCRIPTION', subscription)
}
