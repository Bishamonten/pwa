
export function ADD_SW_REGISTRATION (state, swRegistration) {
  state.swRegistration = swRegistration
}

export function ADD_SUBSCRIPTION (state, subscription) {
  state.subscription = subscription
}
