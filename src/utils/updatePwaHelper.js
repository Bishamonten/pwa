
/**
 * Check if update are available
 */
function checkServiceWorkerUpdates () {
  navigator.serviceWorker.getRegistrations()
    .then(registrationsArray => {
      registrationsArray[0].update()
    })
}

/**
 * Contin Check for new updates and propose to install right away with a modal
 */
function checkPeriodicUpdate () {
  setInterval(checkServiceWorkerUpdates, 60)
}

export default {
	checkServiceWorkerUpdates,
	checkPeriodicUpdate
};
