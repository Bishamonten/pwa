/**
 * Convert a base 64 String to a Uint8Array
 * @param {String} base64String The string in base 64
 */
function urlBase64ToUint8Array (base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4)
  const base64 = (base64String + padding)
  // eslint-disable-next-line no-useless-escape
    .replace(/\-/g, '+')
    .replace(/_/g, '/')

  const rawData = window.atob(base64)
  return Uint8Array.from([...rawData].map((char) => char.charCodeAt(0)))
}
/**
 * Return true if the navigator support service workers and notification.
 */
function arePushNotificationSupported () {
  return ('serviceWorker' in navigator) && ('PushManager' in window)
}
/**
 * Create a notification from the current page.
 * @param {ServiceWorkerRegistration} swRegistration The registration of the service worker
 * @param {NotificationOptions} opt The option of the notification
 * @param {String} title The title of the notification
 * @see https://developer.mozilla.org/fr/docs/Mozilla/Add-ons/WebExtensions/API/notifications/NotificationOptions
 */
function showNotification (swRegistration, title, opt) {
  if (Notification.permission === 'granted') {
    swRegistration.showNotification(title, opt)
  }
}
/**
 * Ask the permission to the user for notifications.
 * @returns {Promise<NotificationPermission>}
 */
function askPermission () {
  return new Promise(
    function (resolve, reject) {
      const permissionResult = Notification.requestPermission(function (result) {
        resolve(result)
      })
      if (permissionResult) {
        permissionResult.then(resolve, reject)
      }
    }).then(function (permissionResult) {
    if (permissionResult !== 'granted') {
      throw new Error('We weren\'t granted permission.')
    }
  })
}
/**
 * Create a subsciption that should be sent to the server.
 */
async function subscribeUserToPush () {
  await navigator.serviceWorker.ready;    
  const registration = await navigator.serviceWorker.register(process.env.SERVICE_WORKER_FILE);
  const subscribeOptions = {
    // The notification won't work if set to false:
    userVisibleOnly: true,
    // Public key of the notification service on the server:
    applicationServerKey: urlBase64ToUint8Array('BJYXhqrmZjxJ5xmgZZ_EZbXDZGXxpijLNa6c35hLT_78BGbpv1cWKKvfjqf1eDYQt1ueXU-UhDDu-Fj-2Angtyw')
  }
  const pushSubscription = await registration.pushManager.subscribe(subscribeOptions)
  console.log('Received PushSubscription: ', JSON.stringify(pushSubscription))
  return { pushSubscription, registration }
}
/**
 * Send the subscription to the back end.
 * @param {PushSubscription} subscription The subscription
 * @example
 * sendSubscriptionToBackEnd(await subscribeUserToPush());
 */
async function sendSubscriptionToBackEnd (subscription) {
  return fetch('https://localhost:5001/api/pushNotifications/subscriptions', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(subscription)
  }).then(function (response) {
    if (!response.ok) {
      throw new Error('Bad status code from server.')
    }

    return response.json()
  }).then(function (responseData) {
    if (!(responseData.data && responseData.data.success)) {
      throw new Error('Bad response from server.')
    }
  })
}

export default {
  urlBase64ToUint8Array,
  arePushNotificationSupported,
  showNotification,
  askPermission,
  subscribeUserToPush,
  sendSubscriptionToBackEnd
}
