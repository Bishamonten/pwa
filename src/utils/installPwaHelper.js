import { Notify } from 'quasar'

// Prompt to install PWA on chrome
function installApp (deferredPrompt) {
  // Show the prompt
    deferredPrompt.prompt()
    // Wait for the user to respond to the prompt
    deferredPrompt.userChoice
      .then((choiceResult) => {
        if (choiceResult.outcome === 'accepted') {
          console.log('PWA setup accepted')
        } else {
          console.log('PWA setup rejected')
        }
      })
  }

  /**
 * Set up event listener to listen beforeinstallprompt and open a notify to install
 */
  function initInstall () {
    // Open a notification if the application is installable
    window.addEventListener('beforeinstallprompt', (e) => {
      // Prevent Chrome 67 and earlier from automatically showing the prompt
        e.preventDefault()
        Notify.create({
          position: 'top-right',
          timeout: 0,
          multiLine: false,
          icon: 'get_app',
          color: 'grey-1',
          textColor: 'grey-10',
          message: 'Installer cette application?',
          actions: [
            { label: 'Installer', color: 'green', handler: () => installApp(e) },
            { icon: 'clear', color: 'black' }
          ]
        })
      })
  }

  export default {
    installApp,
    initInstall
  };
  