/*
 * This file (which will be your service worker)
 * is picked up by the build system ONLY if
 * quasar.conf > pwa > workboxPluginMode is set to "InjectManifest"
 */

import { precacheAndRoute } from 'workbox-precaching/precacheAndRoute'
import {registerRoute} from 'workbox-routing';
import {NetworkFirst} from 'workbox-strategies';
import {StaleWhileRevalidate} from 'workbox-strategies';
import {ExpirationPlugin} from 'workbox-expiration';
import {initialize as pbsInitialize} from './periodic-background-sync.js';

precacheAndRoute(self.__WB_MANIFEST)


let backgroundSyncSupported = 'sync' in self.registration ? true : false
console.log('backgroundSyncSupported: ',  ) 


registerRoute(
  /(.*)\.(?:png|gif|jpg|svg)/,
  new NetworkFirst({
    cacheName: 'bm-images-cache-v1',
    plugins: [
      new ExpirationPlugin({
        maxEntries: 50,
        maxAgeSeconds: 30 * 24 * 60 * 60 
      })
    ]
  })
)

registerRoute(
  new RegExp('.*.css'),
  new NetworkFirst({
    cacheName: 'bm-css-cache-v1',
    plugins: [
      new ExpirationPlugin({
        maxEntries: 50,
        maxAgeSeconds: 10 * 24 * 60 * 60
      })
    ]
  })
)

registerRoute(
  new RegExp('.*.js'),
  new StaleWhileRevalidate({
    cacheName: 'bm-js-cache-v1',
    maxAgeSeconds: 5 * 24 * 60 * 60
  })
)

registerRoute(
  new RegExp('^http'),
  new NetworkFirst()
)

registerRoute(
  new RegExp('^https'),
  new NetworkFirst()
)


// Event needed to update the PWA service worker
self.addEventListener('message', function (event) {
  if (event.data.action === 'skipWaiting') {
    // Used to skip the waiting of the update of the service worker
    self.skipWaiting()
  }
})

/**
 * push event handler.
 * Triggered when the server send a notification.
 */
self.addEventListener('push', function (event) {
  if (event && event.data) {
    //console.log(event.data)
    // Get the payload as an object:
    const data = event.data.json()
    // Show the notification on sreen:
    event.waitUntil(self.registration.showNotification(data.Title, {
      tag: data.Tag,
      body: data.Body,
      actions: [{
        title: 'Valider',
        action: 'ok'
      }, {
        title: 'Refuser',
        action: 'ko'
      }],
      icon: data.Icon || null
    }))
  }
})

/**
 * notificationclick event handler.
 * The triggered action can be find in event.action
 */
self.addEventListener('notificationclick', function (event) {
  // console.log('On notification click: ', event.notification.tag)
  // console.log(event)
  // Close the notification:
  event.notification.close()
})

console.log("INITIALIZE: ", pbsInitialize)

pbsInitialize()