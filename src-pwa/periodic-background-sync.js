import {API_CACHE_NAME, DEFAULT_SORT, DEFAULT_TAG, PBS_TAG}
  from './constants.js';

export async function initialize() {
  console.log("PORCA TROIA")
  console.log(self)
  if ('periodicSync' in self.registration) {
    self.addEventListener('periodicsync', (event) => {
      if (event.tag === PBS_TAG) {
        event.waitUntil((async () => {
          const cache = await caches.open(API_CACHE_NAME);
          const url = "test"
          await cache.add(url);
          console.log(`In periodicsync handler, updated`, url);
        })());
      }
    });

    const status = await self.navigator.permissions.query({
      name: 'periodic-background-sync',
    });

    if (status.state === 'granted') {
      const tags = await self.registration.periodicSync.getTags();
      if (tags.includes(PBS_TAG)) {
        console.log(`Already registered for periodic background sync with tag`,
            PBS_TAG);
      } else {
        try {
          await registration.periodicSync.register(PBS_TAG, {
            // An interval of one day.
            minInterval: 24 * 60 * 60 * 1000,
          });
          console.log(`Registered for periodic background sync with tag`,
              PBS_TAG);
        } catch (error) {
          console.error(`Periodic background sync permission is 'granted', ` +
              `but something went wrong:`, error);
        }
      }
    } else {
      console.info(`Periodic background sync permission is not 'granted', so ` +
          `skipping registration.`);
    }
  } else {
    console.log(`Periodic background sync is not available in this browser.`);
  }
}
